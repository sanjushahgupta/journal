
package utilities;
import android.app.Application;

public class JournalAppi  extends Application {

    private String username;
    private String userId;
    private static JournalAppi instance;

    public JournalAppi() {
    }

    public static JournalAppi getInstance() {
        if (instance == null)
            instance = new JournalAppi();

        return instance;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
