package com.example.journal;

import static com.example.journal.PostJournalAcitivity.hidekeyboard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import android.view.Gravity;

import utilities.JournalAppi;

public class Login_Activity extends AppCompatActivity {
    private Button login_button, create_account_button;
    public AutoCompleteTextView emailAdress;
    private EditText password;
    SharedPreferences sp;
    private TextView forgetpasswordd;

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseAuth currentUser;

    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private final CollectionReference collectionReference = db.collection("Users");
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_login);

        firebaseAuth = FirebaseAuth.getInstance();
        emailAdress = findViewById(R.id.email);
        password = findViewById(R.id.password);
        login_button = findViewById(R.id.email_sign_in);
        create_account_button = findViewById(R.id.Create_an_account_button);
        sp=getSharedPreferences("login", Context.MODE_PRIVATE);
forgetpasswordd=findViewById(R.id.forgetpassword);


        create_account_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login_Activity.this, CreateAccountActivity.class));
            }
        });


        emailAdress.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    InputMethodManager imm=(InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(),0);
                }
            }
        });

password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(!hasFocus){
            InputMethodManager imm=(InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(),0);
        }
    }
});
   forgetpasswordd.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        sendPasswordReset();
    }
});
        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hidekeyboard(Login_Activity.this);
                if (emailAdress.getText().toString().matches(emailPattern)){

                }else{
                    Toast.makeText(Login_Activity.this, "Invalid Email", Toast.LENGTH_SHORT).show();
                }
                loginEmailPasswordUser(emailAdress.getText().toString().trim(),
                        password.getText().toString().trim());
                sp.edit().putBoolean("logged",true).apply();

            }




            private void loginEmailPasswordUser(String email, String pwd) {

                if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(pwd)) {
                    firebaseAuth.signInWithEmailAndPassword(email, pwd)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        FirebaseUser user = firebaseAuth.getCurrentUser();
                                        assert user != null;
                                        final String currentUserId = user.getUid();


                                        collectionReference.whereEqualTo("userId", currentUserId)
                                                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                                    @Override
                                                    public void onEvent(@Nullable QuerySnapshot querySnapshot, @Nullable FirebaseFirestoreException e) {

                                                        if (e != null) {

                                                        }
                                                        assert querySnapshot != null;
                                                        if (!querySnapshot.isEmpty()) {
                                                            for (QueryDocumentSnapshot snapshot : querySnapshot) {
                                                                JournalAppi journalapi = JournalAppi.getInstance();
                                                                journalapi.setUsername(snapshot.getString("userame"));
                                                                journalapi.setUserId(snapshot.getString("userId"));

                                                                //gO TO LISTACTIVITY
                                                                startActivity(new Intent(Login_Activity.this, JournalListActivity.class));

                                                            }
                                                        } else {
                                                            Toast.makeText(Login_Activity.this, "Invalid emailuu or password", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });

                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                            Toast.makeText(Login_Activity.this, "Email address and password donot match.",
                                    Toast.LENGTH_LONG).show();
                        }
                    });


                } else {

                    Toast toast = Toast.makeText(Login_Activity.this, "Please fill in email address and password.", Toast.LENGTH_SHORT);




                }

            }



        });






    }

    public void sendPasswordReset() {
        // [START send_password_reset]
        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (!TextUtils.isEmpty( emailAdress.getText().toString()) && emailAdress.getText().toString().matches(emailPattern)) {


            auth.sendPasswordResetEmail(emailAdress.getText().toString().trim())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(Login_Activity.this, "The link to reset the password is sent to your email.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

        }else{

            Toast.makeText(Login_Activity.this, "Please enter your email address.", Toast.LENGTH_SHORT).show();
        }

    }




}