package com.example.journal;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import model.Journal;
import ui.JournalRecycleradapter;
import utilities.JournalAppi;

public class JournalListActivity extends AppCompatActivity {

    AlertDialog.Builder builder;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
  //  private FirebaseUser user;
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private StorageReference storageReference;
    private List<Journal> journalList;
    private RecyclerView recyclerView;
    private JournalRecycleradapter journalRecycleradapter;
    private Button Editbutton;
    private final CollectionReference collectionReference = db.collection("Journal");
    private TextView noJournalEntry;
    private SharedPreferences sp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journal_ist2);
        firebaseAuth = FirebaseAuth.getInstance();
      //  user = firebaseAuth.getCurrentUser();
        noJournalEntry = findViewById(R.id.nothoughs);
        journalList = new ArrayList<>();


        builder = new AlertDialog.Builder(this);

        recyclerView = findViewById(R.id.recyclerView2);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        //sign user out


    }
    public void onBackPressed() {
      Toast.makeText(JournalListActivity.this,"You are at home page",Toast.LENGTH_SHORT).show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_add:
                //take user to add journal
                 {
                     startActivity(new Intent(JournalListActivity.this, PostJournalAcitivity.class));

                }
                break;

            case R.id.actionsignout: {
                builder.setMessage("Do you want to Logout ?")
                        .setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        firebaseAuth.signOut();
                        Intent intent = (new Intent(JournalListActivity.this, MainActivity.class));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                        finish();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert = builder.create();
                alert.setTitle("Logout");
                alert.show();


            }
            break;

          case R.id.deleteicon: {
              startActivity(new Intent(JournalListActivity.this, Delete_Activity.class));

          }


            break;




        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStart() {
        super.onStart();

        collectionReference.whereEqualTo("userId", JournalAppi.getInstance().getUserId())
                .get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot querySnapshot) {
                if (!querySnapshot.isEmpty()) {
                    for (QueryDocumentSnapshot journals : querySnapshot) {
                        Journal journal = journals.toObject(Journal.class);
                        journalList.add(journal);
                    }



                    //invoke recyclerview
                    journalRecycleradapter = new JournalRecycleradapter(JournalListActivity.this, journalList);
                    recyclerView.setAdapter(journalRecycleradapter);
                    journalRecycleradapter.notifyDataSetChanged();

                } else {
                    Toast.makeText(JournalListActivity.this,"Please add journal to see your list.",Toast.LENGTH_SHORT).show();
                    noJournalEntry.setVisibility(View.VISIBLE);

                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });


    }
}