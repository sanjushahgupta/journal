package com.example.journal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.Date;

import model.Journal;
import utilities.JournalAppi;

public class PostJournalAcitivity<val> extends AppCompatActivity {

    private static final int GALLERY_CODE = '1';
    Uri imageuri;
    //connect to firestore
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    private Button saveButton;
    private ImageView addphoto;
    private EditText titleedittext;
    private EditText thoughtsedit;
    private TextView currentusernametextview;
    private Button postsavebutton;
    private ImageView imageview33;
    ActivityResultLauncher<String> mGetContent = registerForActivityResult(
            new ActivityResultContracts.GetContent(),
            new ActivityResultCallback<Uri>() {
                @Override
                public void onActivityResult(Uri result) {
                    if (result != null) {
                        imageview33.setImageURI(result);
                        imageuri = result;
                    } else {
                        startActivity(new Intent(PostJournalAcitivity.this, PostJournalAcitivity.class));
                        Toast.makeText(PostJournalAcitivity.this, "please inse", Toast.LENGTH_SHORT).show();
                    }
                }
            }
    );

    private String currentUserId;
    private String CurrentUserName;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseUser user;
    private StorageReference storageReference;
    private final CollectionReference collectionReference = db.collection("Journal");

    public static void hidekeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(
                Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager.isAcceptingText()) {
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0
            );
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.activity_post_journal_acitivity);

        firebaseAuth = FirebaseAuth.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();

        addphoto = findViewById(R.id.postCameraButton);
        titleedittext = findViewById(R.id.Post_title_edit);
        thoughtsedit = findViewById(R.id.post_description);
        currentusernametextview = findViewById(R.id.post_username_textview);
        postsavebutton = findViewById(R.id.post_save_journal);
        imageview33 = findViewById(R.id.imageView2);

        thoughtsedit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    InputMethodManager imm=(InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(),0);
                }
            }
        });

        titleedittext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    InputMethodManager imm=(InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(),0);
                }
            }
        });



        if (JournalAppi.getInstance() != null) {
            currentUserId = JournalAppi.getInstance().getUserId();
            CurrentUserName = JournalAppi.getInstance().getUsername();
            currentusernametextview.setText(CurrentUserName);


        }

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();

                if (user != null) {


                } else {

                }
            }
        };





        postsavebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PostJournalAcitivity.this, "In progress", Toast.LENGTH_SHORT).show();
                hidekeyboard(PostJournalAcitivity.this);
                saveJournal();
                Log.d("JournalListDebug", "Saved");
            }
        });

        addphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (v.getId() == R.id.postCameraButton) {//get image from gallery
                    mGetContent.launch("image/*");

                } else {
                    Toast.makeText(PostJournalAcitivity.this, "something went wrong ", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
    @Override
    public void onBackPressed() {
        Intent intent= new Intent(PostJournalAcitivity.this,JournalListActivity.class);
        startActivity(intent);
    }


    private void saveJournal() {
        final String title = titleedittext.getText().toString().trim();
        final String thoughts = thoughtsedit.getText().toString().trim();

        //   progressBar.setVisibility(View.VISIBLE);
        if (TextUtils.isEmpty(title) && TextUtils.isEmpty(thoughts) && imageuri == null) {
            Toast.makeText(PostJournalAcitivity.this, "You have not inserted anything. So unable to save.", Toast.LENGTH_SHORT).show();
        }

        if (imageuri != null) {
            StorageReference filepath = storageReference.child("journal_images")
                    .child("images_" + Timestamp.now().getSeconds());

            filepath.putFile(imageuri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    //create a journal object
                    filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String imageUrl = uri.toString();
                            saveJournalText(imageUrl, title, thoughts);
                        }
                    });
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(PostJournalAcitivity.this, "cannt save duee", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else if (!TextUtils.isEmpty(title) || !TextUtils.isEmpty(thoughts)) {
            saveJournalText("", title, thoughts);
        }
    }


    ////save a journal instance.
    @Override
    protected void onStart() {
        super.onStart();
        user = firebaseAuth.getCurrentUser();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (firebaseAuth != null) {
            firebaseAuth.removeAuthStateListener(authStateListener);
        }
    }

    private void saveJournalText(String uri, String title, String thoughts) {
        Journal journal = new Journal();

        if (uri.length() > 0) {
            journal.setImageUrl(uri);
        }

        journal.setTitle(title);
        journal.setThought(thoughts);

        journal.setTimeAdded(new Timestamp(new Date()));
        journal.setUsername(CurrentUserName);
        journal.setUserId(currentUserId);

        //invoke our collectionReferences
        collectionReference.add(journal)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(PostJournalAcitivity.this, "saved", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(PostJournalAcitivity.this, JournalListActivity.class));
                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }
}
