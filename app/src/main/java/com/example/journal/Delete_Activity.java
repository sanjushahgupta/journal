package com.example.journal;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;






        import androidx.annotation.NonNull;
        import androidx.appcompat.app.AppCompatActivity;

        import android.os.Bundle;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.Toast;

        import com.google.android.gms.tasks.OnCompleteListener;
        import com.google.android.gms.tasks.OnFailureListener;
        import com.google.android.gms.tasks.OnSuccessListener;
        import com.google.android.gms.tasks.Task;
        import com.google.firebase.firestore.DocumentSnapshot;
        import com.google.firebase.firestore.FirebaseFirestore;
        import com.google.firebase.firestore.QuerySnapshot;
public class Delete_Activity extends AppCompatActivity {


    FirebaseFirestore db= FirebaseFirestore.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        EditText usernametodelete;
        Button confirm;


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete);
        usernametodelete=findViewById(R.id.username_delete_txt);
        confirm=findViewById(R.id.confirm_btn);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostJournalAcitivity.hidekeyboard(Delete_Activity.this);
                String namee=usernametodelete.getText().toString();
                if(!namee.isEmpty()){
                    Deleteitem(namee);
                }else{
                    Toast.makeText(Delete_Activity.this, "Please fill Username field.", Toast.LENGTH_SHORT).show();
                }

            }
        });




    }
    private  void Deleteitem(String name){

        db.collection("Journal").whereEqualTo("username",name)
                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful() && !task.getResult().isEmpty()){

                    DocumentSnapshot documentSnapshot= task.getResult().getDocuments().get(0);
                    String documentid =documentSnapshot.getId();
                    db.collection("Journal").document(documentid)
                            .delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void unused) {
                            Toast.makeText(Delete_Activity.this,"Successfully Deleted",Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    Toast.makeText(Delete_Activity.this,"List is empty.",Toast.LENGTH_SHORT).show();
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });

    }
}