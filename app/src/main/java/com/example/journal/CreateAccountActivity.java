package com.example.journal;

import static com.example.journal.PostJournalAcitivity.hidekeyboard;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import utilities.JournalAppi;

public class CreateAccountActivity extends AppCompatActivity {
private EditText Ca_Emmail;
    private EditText Ca_Username;
    private EditText Ca_password;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseUser currentUser;
    private ProgressBar Ca_progressBar;
    private  Button Ca_createanaccount;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    //Firestore connection
    private  FirebaseFirestore db= FirebaseFirestore.getInstance();
    private CollectionReference collectionReference= db.collection("Users");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        firebaseAuth= FirebaseAuth.getInstance();
        Ca_Emmail=findViewById(R.id.Account_email);
        Ca_Username=findViewById(R.id.username_account);
        Ca_password=findViewById(R.id.Account_password);
        Ca_createanaccount = findViewById(R.id.Create_an_account);

        Ca_Username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    InputMethodManager imm=(InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(),0);
                }
            }
        });

        Ca_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    InputMethodManager imm=(InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(),0);
                }
            }
        });

         Ca_Emmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    InputMethodManager imm=(InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(),0);
                }
            }
        });




        authStateListener= new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                //currentuser is still same
                currentUser= firebaseAuth.getCurrentUser();

                if(currentUser !=null){
                    //user is already loggedin...

                }else{
                    //no user yet...
                }

            }


        };

        Ca_createanaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hidekeyboard(CreateAccountActivity.this);


                if (!TextUtils.isEmpty(Ca_Emmail.getText().toString()) ){
                    if(!Ca_Emmail.getText().toString().matches(emailPattern)){
                        Toast.makeText(CreateAccountActivity.this, "Invalid Email", Toast.LENGTH_SHORT).show();
                    }

                }
                if(!TextUtils.isEmpty(Ca_Emmail.getText().toString())
                && !TextUtils.isEmpty(Ca_password.getText().toString())
                && !TextUtils.isEmpty(Ca_Username.getText().toString())){

                   String email=Ca_Emmail.getText().toString();
                            String password=Ca_password.getText().toString();
                           String username=Ca_Username.getText().toString();




                           if(password.length() <= 5){

                        Toast.makeText(CreateAccountActivity.this, "Password must be atleast of 6 letters", Toast.LENGTH_SHORT).show();
                    }createUserEmailAccount(email,password, username);

                }else if(TextUtils.isEmpty(Ca_Username.getText())|| TextUtils.isEmpty(Ca_Emmail.getText())|| TextUtils.isEmpty(Ca_password.getText())){
                    Toast.makeText(CreateAccountActivity.this, "Please fill in all fields.", Toast.LENGTH_SHORT).show();
                }




            }
        });
    }


   private void createUserEmailAccount(String email, String password, final String username) {
       if (!TextUtils.isEmpty(email)
               && !TextUtils.isEmpty(password)
               && !TextUtils.isEmpty(username))  {



           firebaseAuth.createUserWithEmailAndPassword(email,password)
                   .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                       @Override
                       public void onComplete(@NonNull Task<AuthResult> task) {

                           if (task.isSuccessful()) {

                               currentUser = firebaseAuth.getCurrentUser();
                               assert currentUser != null;
                               String currentUserId = currentUser.getUid();

                               //create a user Map so we can create a user in the user collection.

                               Map<String, String> userobj = new HashMap<>();
                               userobj.put("userId", currentUserId);
                               userobj.put("userame",username);

                               // save to our firestore database
                               collectionReference.add(userobj)
                                       .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                           @Override
                                           public void onSuccess(DocumentReference documentReference) {
                                               documentReference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                   @Override
                                                   public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                                                       if (task.getResult().exists()) {


                                                           String name=task.getResult().getString("username");

                                                           JournalAppi journalAppi = JournalAppi.getInstance();
                                                           journalAppi.setUserId(currentUserId);
                                                           journalAppi.setUsername(name);

                                                           Toast.makeText(CreateAccountActivity.this, "Account created", Toast.LENGTH_SHORT).show();
                                                          // sendEmailVerification();
                                                          Intent intent= new Intent(CreateAccountActivity.this,Login_Activity.class);
                                                           //intent.putExtra("username", name);
                                                          // intent.putExtra("userId",currentUserId);
                                                           startActivity(intent);

                                                       }else{
                                                          // Toast.makeText(CreateAccountActivity.this, "Account exists", Toast.LENGTH_SHORT).show();




                                                       }
                                                   }
                                               });
                                           }

                                       });


                               //we take user to add activities
                           }else {

                               firebaseAuth.fetchSignInMethodsForEmail(Ca_Emmail.getText().toString())
                                       .addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
                                           @Override
                                           public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {
                                               boolean isNewUser = task.getResult().getSignInMethods().isEmpty();
                                               if (!isNewUser) {
                                                   Toast.makeText(CreateAccountActivity.this, "This email address has been taken by another user.", Toast.LENGTH_SHORT).show();
                                               }
                                           }
                                       });
                           }

                       }
                   });


       }

   }


   /* public void sendEmailVerification() {
        // [START send_email_verification]
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();

        user.sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                         Toast.makeText(CreateAccountActivity.this,"sent a link to your Email address", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    */

    @Override
    protected void onStart() {
        super.onStart();
        currentUser= firebaseAuth.getCurrentUser();
        firebaseAuth.addAuthStateListener(authStateListener);
    }
}
