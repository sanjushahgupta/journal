package ui;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.journal.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import model.Journal;

public class JournalRecycleradapter extends RecyclerView.Adapter<JournalRecycleradapter.ViewHolder>{
 private Context context;
 private List<Journal>journalList;

    public JournalRecycleradapter(Context context, List<Journal> journalList) {
        this.context = context;
        this.journalList = journalList;
    }

    @NonNull
    @Override
    public JournalRecycleradapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
       View view= LayoutInflater.from(context).inflate(R.layout.journal_row,viewGroup,false);
        return new ViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(@NonNull JournalRecycleradapter.ViewHolder viewHolder, int position) {
  Journal journal= journalList.get(position);
  String imageUrl;
  imageUrl=journal.getImageUrl();

  String title=journal.getTitle();
  if(title.length()==0)
  {
      title="Title is empty.";
  }
  String thoughts= journal.getThought();
  if(thoughts.length()==0){
      thoughts="Thought is empty.";
  }
  viewHolder.title.setText(title);
  viewHolder.thoughts.setText(thoughts);
  viewHolder.name.setText(journal.getUsername());

 //time-ago
        String timeAgo=(String) DateUtils.getRelativeTimeSpanString(journal.getTimeAdded().getSeconds()
                * 1000);

viewHolder.dateAdded.setText(timeAgo);



/* picasso library to download and show images */
        Picasso.get().load(imageUrl).placeholder(R.drawable.def).fit().centerCrop()
                .into(viewHolder.image);



    }

    @Override
    public int getItemCount() {
        return journalList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView title, thoughts, dateAdded,name;
        public ImageView image;
        String userId;
        String userName;


        public ViewHolder(@NonNull View itemView, Context ctx) {

            super(itemView);
            context= ctx;
            title=itemView.findViewById(R.id.journal_title_list);
            thoughts=itemView.findViewById(R.id.journal_thought_list);
            dateAdded=itemView.findViewById(R.id.journal_timestamp_list);
            image=itemView.findViewById(R.id.journal_image_list);
            name=itemView.findViewById(R.id.journal_row_username);
        }
    }
}

