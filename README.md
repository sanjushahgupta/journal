With this journal app you can save your thoughts, ideas and feelings.

you can attach photos to an entry.

You can track your notes to know when you wrote them and can delete if necessary.

If you forget your password, you can reset it and set new password to access the account.
